#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

#include "parser.h"
#include "gnuplot_i.hpp"

/*---------------------------------------------------------//
// Function STR2NUM: function (templated) converts a       // 
//                   string to a numerical value           //
//                                                         //
// Arguments       :  s=> pointer to the string to be      //
//                        converted                        //
//---------------------------------------------------------*/

template <class numtype>
numtype str2num(string &s)
{
    numtype num;

    stringstream(s) >> num;
    
    return num;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function NUM2STR: function (templated) converts a       //
//                   numerical value to a string           //
//                                                         //
// Arguments       :num  => pointer to the numerical value //
//                          to be converted                //
//                 :prstn=> ponter to the precision of the //
//                          floating point                 //
//---------------------------------------------------------*/

template <class numtype>
string num2str(numtype &num, int &prstn, string &sci)
{
    stringstream conv;
    string str;
    
    if (sci == "fixed")
    {
        conv << fixed << setprecision(prstn) << num << '\n';
    } else
    {
        conv << scientific << setprecision(prstn) << num << '\n';
    }
    getline(conv,str);

    return str;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function ISNUM: function tests a string for a numerical //
//                  value (Templated for the type for      //
//                  testting).                             //
//                                                         //
// Arguments     : s => pointer to the string to be        //
//                      tested                             //
//---------------------------------------------------------*/

template <class numtype>
bool isnum(string &s1)
{
    istringstream iss(s1);
    /* numtype can be int, float and double;
       non-numeric types always will be false*/
    numtype val;
    /* reads from the stream; noskipws considers 
       invalid any leading whitespaces*/
    iss >> noskipws >> val;
    /* tests the stream for being finished
       and failure to pass the value*/
    return iss.eof() && !iss.fail();
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GENERATE_VECTOR: generates a float vector      //
//                           (templated for the integer    //
//                           type).                        //
//                                                         //
// Arguments               : n => pointer to the inttype   //
//                                integer                  //
//---------------------------------------------------------*/

template <class inttype, class T>
vector<T> generate_vector(inttype &n)
{
    int val = 0, k = 0, vecsize = 0;
    T valf = 0.0;
    vector<T> valvec, fvec;

    for (int i = 1; i <= n; i++) 
    {
        val = rand() % n;
        valf = val*1.0/n;
        for (int j = 0; j < valvec.size(); j++)
        {
            if (valf == valvec[j]) 
            {
                val  = rand() % n;
                valf = val*1.0/n;
                j = -1;
            }
        }
        valvec.push_back(valf);
    }
    valvec.push_back(1);

    vecsize = valvec.size();
    for (int i = 0; i < vecsize; i++)
    {
        valf = valvec[k];
        for (int j = 0; j < valvec.size(); j++) 
        {
            if (valf > valvec[j]) 
            {
                valf = valvec[j];
                k = j;
            }
        }
        fvec.push_back(valf);
        valvec.erase(valvec.begin()+k);
        k = 0;
    }

    return fvec;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function READ_HEADER: function reads the header of a    //
//                       data file.                        //
//                                                         //
// Arguments           : s1  => pointer to the string      //
//                              containing the data        //
//                              filename                   //
//                       s2  => pointer to the string      //
//                              containing the field name  //
//                       num => number of fields           //       
//---------------------------------------------------------*/

int read_header(string &s1, string &s2, int num)
{
    char charstrm;
    int tindex = 0, j = 0, n = 0;
    ifstream file(s1.c_str());
    stringstream iss;
    string line = "", field = "", strstrm = "", auxstr = "";
    parser token;
    vector<string> header;

    getline(file,line);
    token.re_init(line);
    /* tests to see if number of tokens in the 
       first line of s1 file is the same as 
       the number of fields; if it is selects
       the s2 field; else finds the right position
       for the s2 field*/
    if (token.ntoken() == num)
    {
        field = token.select_token(0);
        while (field != s2)
        {
            j++;
            field = token.select_token(j);
        }
    } else
    {
        iss << line;
        for (int i = 0; i < token.ntoken(); i++)
        {
            field = token.select_token(i);
            if (i == 0)
            {
                n = field.size();
            } else {n = field.size() - 1;}
            for (int k = 1; k <= n; k++) {iss.get(charstrm);}
            iss.get(charstrm);
            strstrm = charstrm;
            while (strstrm == " ")
            {
                iss.get(charstrm);
                strstrm = charstrm;
                j++;
            }
            if (j == 1)
            {
                do
                {
                    field = field+" "+token.select_token(i+1);
                    j = 0;
                    i++;
                    auxstr = token.select_token(i);
                    if (i == 0)
                    {
                        n = auxstr.size();
                    } else {n = auxstr.size() - 1;}
                    for (int k = 1; k <= n; k++) {iss.get(charstrm);}
                    iss.get(charstrm);
                    strstrm = charstrm;
                    while (strstrm ==" ")
                    {
                        iss.get(charstrm);
                        strstrm = charstrm;
                        j++;
                    }
                } while (j == 1);
                header.push_back(field);
                j = 0;
            } else
            {
                header.push_back(field);
                j = 0;
            }
        }
        j = 0;
        for (int l = 0; l < header.size()-1; l++)
        {
            if (header[l] == s2) {j = l;}
        }
    }

    file.close();

    tindex = j;

    return tindex;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_HEADER_SIZE: function gets the number of   //
//                           file.                         //
//                                                         //
// Arguments               : s => pointer to the string    //
//                                 containing the data     //
//                                 filename                //
//---------------------------------------------------------*/

int get_header_size(string &s)
{
    int hsize = 0;
    /* isheader is true if reading header from s file
       or false if reading from data in s file*/
    bool isheader = true;
    ifstream file(s.c_str());
    string line = "", field;
    parser token;

    /* reads a line from s file until file is over
       or if isheader is false*/
    while (getline(file,line) || isheader)
    {
        token.re_init(line);
        field = token.select_token(0);
        /* tests to see if the new line is of data or
           header; if data isheader is false*/
        if (isnum<float>(field)) 
        {
            isheader = false;
        } else
        {
            hsize++;
        }
        
    }

    file.close();

    return hsize;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_DATA_SIZE: function gets the number of     //
//                         data points                     //
//                                                         //
// Arguments             : s   => pointer to the string    //
//                                containing the data      //
//                                filename                 //
//                         num => header size              //
//---------------------------------------------------------*/

int get_data_size(string &s, int &num)
{
    int dat_size = 0, size = 0;
    string garbage = "";
    ifstream file(s.c_str());
    /* reads the entire file and defines the number of lines */
    while (getline(file,garbage)) {size++;}
    
    dat_size = size - num;
    file.close();
    
    return dat_size;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_FIELD_NUMBER: function gets the number of  //
//                            fields.                      //
//                                                         //
// Arguments                : s   => pointer to the string //
//                                   containing the data   //
//                                   filename              //
//                            num => header size           //
//---------------------------------------------------------*/

int get_field_number(string &s, int num)
{
    ifstream file(s.c_str());
    string line = "";
    parser token;

    /* reads header and discards it*/
    for (int i = 0; i <= num-1; i++) {getline(file,line);}

    /* reads the first data line in s file;
       counts the number of tokens in line*/
    getline(file,line);
    token.re_init(line);

    file.close();

    return token.ntoken();
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_FIELD_VAL: reads data from a specific      //
//                         position on a named field       //
//                         in a data file (templated)      //
// Arguments             : s1  => pointer to the string    //
//                                containing the data      //
//                                filename                 //
//                         num1=> header size              //
//                         num2=> field identifier         //
//                         num3=> position on field        //
//---------------------------------------------------------*/

template <class numtype>
numtype get_field_val(string &s, int &num1, int num2, int &num3)
{
    ifstream file(s.c_str());
    numtype val;
    string line = "", val_str = "", header = "";
    parser token;
    /* reads and discards the header */
    for (int i = 1; i <= num1; i++){getline(file,header);}
    /* reads and discards the data points prior to num3 */
    for (int i = 1; i < num3; i++){getline(file,line);}
    line = "";
    
    getline(file,line);
    token.re_init(line);
    val_str = token.select_token(num2);
    stringstream(val_str) >> val;
    file.close();    

    return val;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function AXIS: reads data from a named field in a data  //
//                file.                                    //
//                                                         //
// Arguments    : s1  => pointer to the string containing  //
//                       the data filename                 //
//                num1=> field identifier                  //
//                num2=> header size                       //
//---------------------------------------------------------*/

vector<double> axis(string &s1, int num1, int num2)
{
    double val = 0.0;
    string line = "", field = "";
    parser token;
    ifstream file(s1.c_str());
    vector<double> vec;

    /* read the the header*/
    for (int i = 1; i <= num2; i++) {getline(file,line);}

    /* get the values from the specific field*/
    while (getline(file,line))
    {
        token.re_init(line);
        field = token.select_token(num1);
        stringstream(field) >> val;
        vec.push_back(val);
    }
    file.close();

    return vec;
} 
/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function FILETEST: function tests the contents of a     //
//                    file.                                //
//                                                         //
// Arguments         : s1  => pointer to the string        //
//                            containing the data filename //
//                     num => header size                  //
//---------------------------------------------------------*/

bool filetest(string &s1, int num)
{
    bool no_err = false;
    ifstream file(s1.c_str());
    string line = "";
    parser token;
    vector <string> vec;
    
    if (file.is_open())
    {
        no_err = true;
        /* reads and discards the header*/
        for (int i = 1; i <= num; i++) {getline(file,line);}
        while (getline(file,line) && no_err)
        {
            token.re_init(line);
            vec = token.tokenize();
            cout << token.ntoken() << '\n';
            for (int j = 0; j < vec.size(); j++)
            {
                if (isnum<float>(vec[j]))
                {
                    no_err = true;
                } else 
                {
                    no_err = false;
                    j = vec.size();
                }
            }
        }
        file.close();
    }

    return no_err;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function COPY_FILE: function copies data files from a   //
//                     location.                           //
//                                                         //
// Arguments         : s1 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//                     s2 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//---------------------------------------------------------*/

void copy_file(string &s1, string &s2)
{
    int i = 0;
    ifstream infile(s1.c_str());
    ofstream outfile(s2.c_str());
    string line, field;
    parser token1;

    while (getline(infile,line))
    {
        token1.re_init(line);
        field = token1.select_token(0);
        outfile << line << '\n';
        i++;
    }

    infile.close();
    outfile.close();
}

/*---------------------------------------------------------*/

int main(int argc, char* argv[])
{
    Gnuplot g1("lines"), g2("linespoints"), g3("linespoints"), g4("linespoints"), g5("lines"), g6, g7("lines"), g8("linespoints"), g9("lines");
    Gnuplot g10("lines");
    /**/
    int auxnf = 0, auxnh = 0, auxfid = 0, m = 0, nf = 0, nh = 0, fid = 0, val = 0, ptype = 7;
    int isize = 256, jsize = 256, ii = 0, pr = 0, dats = 0;
    long int vecsize = 100000;
    /**/
    double udens = 1.92E-23, hmass = 1.6733E-24, umass = 5.64E+40, utime = 9.79E+7;
    double valor = 0.0, rotcurve = 0.0;
    double alpha = 240.0, beta = 120.0, gamma = 3.4, delta = 360.0, epsilon = 3.1;
    double ni = 0.09, omegap = 25.0;
    double gconst = 6.67259E-08, smass = 20.0*umass, pi = 3.14, potval = 0.0, sigma = 0.2;
    double mass0 = 50.0*umass, epsilons = 2.5, massR = 0.0;
    /**/
    string line = "", file = "", fname = "statistics_01", figname = "dens_evol";
    string ofile = "stat.dat", newstr = "_sig", exten = ".dat", multifig = "dens";
    string sig = "{/Symbol s}", dirstr = "", figexten = ".eps";
    string auxfile = "stat.txt", line1 = "", line2 = "", tokenstr, line3 = "";
    string sigstr = "", smassstr = "", fstr = "", fname2 = "stat", sci = "scientific";
    string home = getenv("HOME"), ep = "{/Symbol e}";
    /**/
    vector<double> xaxis, yaxis, tempvec, var, fieldval, initval, pot, teta;
    vector<double> epsis;
    vector<string> wfield, figchar, varchar2, varchar3, varchar4;
    /**/
    stringstream filestrm, conv_int;
    ofstream output("teste.txt");
    ifstream auxstrm(auxfile.c_str()), mfile;
    /**/
    parser token, token2, token3;

    for (int j = 1; j < argc; j++)
    {
        filestrm << argv[j] << '\n';
        getline(filestrm,line);
        wfield.push_back(line);
        cout << line << '\n';
    }


    file = fname;

    g1.set_font("{},14");
    figname = figname;
    dirstr = home + "/data_files2/";

/* Densidade pelo tempo para sig 100 e sig 200 */
    for (int i = 100; i <= 100; i = i + 100)
    {
        cout << i << '\n';
        file = file + newstr;
        line = "";
        conv_int << i << '\n';
        getline(conv_int,line);
        file = file + line;
        file = file + exten;
        nh = get_header_size(file);
        nf = get_field_number(file,nh);
        cout << file << '\n';
        fid = read_header(file,wfield[0],nf);
        cout << fid << "    " << nf << "     " << nh << '\n';
        xaxis = axis(file,fid-1,nh);
        fid = read_header(file,wfield[1],nf);
        cout << fid << "    " << nf << "     " << nh << '\n';
        yaxis = axis(file,fid-1,nh);
        for (int i = 0; i < xaxis.size(); i++) 
        {
            xaxis[i] = xaxis[i]*utime/1.0E+9;
            yaxis[i] = yaxis[i]*udens/hmass;
            output << xaxis[i]*utime/1.0E+9 << "            " << yaxis[i]*udens/hmass << '\n';
        }
        output << "" << '\n';
        output << "" << '\n';
        if (!xaxis.empty() || !yaxis.empty())
        {
            g1.savetops(figname);
            g1.set_xlabel("t (Gyrs)","");
            g1.set_ylabel("n_{max} (cm^{-3})","");
            g1.set_x_format();
            g1.set_y_format();
            g1.plot_xy(xaxis,yaxis,sig+" = "+line+" pc","");
        } else
        {
            cout << "Error while reading " << file << "." << '\n';
            cout << "One or more fields is empty!" << '\n';
        }
        file = "";
        file = fname;
        xaxis.clear();
        yaxis.clear();
    }
    file = "";
    line = "";

    output.close();

    g2.set_font("{},14");
    g3.set_font("{},14");
    g4.set_font("{},14");
    g5.set_font("{},14");
    g6.set_font("{},14");
    g7.set_font("{},14");
    g8.set_font("{},14");
    g9.set_font("{},14");

/* Densidade pela pitch angle para sig 100 e sig 200 */
    getline(auxstrm,line);
    auxstrm.close();

    auxnh = get_header_size(auxfile);
    auxnf = get_field_number(auxfile,nh);
    cout << auxnf << "      " << auxnh << '\n';
    token.re_init(line);
    tokenstr = token.select_token(0);
    auxfid = read_header(auxfile,tokenstr,auxnf);
    var = axis(auxfile,auxfid,auxnh);

    for (int i = 0; i < token.ntoken(); i++) {figchar.push_back(token.select_token(i));}
    multifig = multifig + "_" + figchar[0];

    line3 = "";

    for (int l = 100; l <= 100; l = l + 100)
    {
        for (int k = ( (int) var[0]); k <= ( (int) var[1]); k = k + ( (int) var[2]))
        {
            conv_int << k << '\n';
            getline(conv_int,line1);
            conv_int << l << '\n';
            getline(conv_int,line2);
            file = dirstr+token.select_token(0)+line1+"/"+token.select_token(1)+"10/"+token.select_token(2)+"50/sig"+line2;
            file  = file + "/" + fname + exten;
            cout << file << '\n';
            copy_file(file,ofile);
            nh = get_header_size(ofile);
            nf = get_field_number(ofile,nh);
            fid = read_header(ofile,wfield[1],nf);
            xaxis = axis(ofile,fid-1,nh);
            fieldval.push_back(xaxis[xaxis.size()-1]);
            initval.push_back(k);
            cout << "=====================" << '\n';
            for (int ii = 0; ii < fieldval.size(); ii++) {cout << initval[ii] << "    " << fieldval[ii] << '\n';}
            cout << "=====================" << '\n';
        }
        for (int i = 0; i < fieldval.size(); i++) {fieldval[i] = fieldval[i]*udens/hmass;}
        g2.savetops(multifig);
        g2.set_xlabel("i (^o)","");
        g2.set_ylabel("n (g cm^{-3})","");
        g2.set_x_format();
        g2.set_y_format();
        g2.set_legend_pos(((int) initval[2]) - 6,((int) fieldval[2]) + 5);
        conv_int << ptype << '\n';
        getline(conv_int,line3);
        g2.plot_xy(initval,fieldval,sig+" = "+line2+" pc","lc -1 pointtype "+line3);
        file = "";
        xaxis.clear();
        fieldval.clear();
        initval.clear();
        ptype = 17;
    } 

/* Densidade pelo velocidade inicial para sig 100 e sig 200 */
    tokenstr = token.select_token(1);
    auxfid = read_header(auxfile,tokenstr,auxnf);
    var = axis(auxfile,auxfid,auxnh);

    multifig = "dens";
    multifig = multifig + "_" + figchar[1];

    ptype = 7;
    line3 = "";
    for (int l = 100; l <= 100; l = l + 100)
    {
        for (int k = ( (int) var[0]); k <= ( (int) var[1]); k = k + ( (int) var[2]))
        {
            conv_int << k << '\n';
            getline(conv_int,line1);
            conv_int << l << '\n';
            getline(conv_int,line2);
            file = dirstr+token.select_token(0)+"15/"+token.select_token(1)+line1+"/"+token.select_token(2)+"50/sig"+line2;
            file  = file + "/" + fname + exten;
            cout << file << '\n';
            copy_file(file,ofile);
            nh = get_header_size(ofile);
            nf = get_field_number(ofile,nh);
            fid = read_header(ofile,wfield[1],nf);
            xaxis = axis(ofile,fid-1,nh);
            fieldval.push_back(xaxis[xaxis.size()-1]);
            initval.push_back(k);
            cout << "=====================" << '\n';
            for (int ii = 0; ii < fieldval.size(); ii++) {cout << initval[ii] << "    " << fieldval[ii] << '\n';}
            cout << "=====================" << '\n';
        }
        for (int i = 0; i < fieldval.size(); i++) {fieldval[i] = fieldval[i]*udens/hmass;}
        g3.savetops(multifig);
        g3.set_xlabel("V_{i} (km s^{-1})","");
        g3.set_ylabel("n (g cm^{-3})","");
        g3.set_x_format();
        g3.set_y_format();
        g3.set_legend_pos(initval[3] - 10.5,fieldval[0] + 300.0);
        conv_int << ptype << '\n';
        getline(conv_int,line3);
        g3.plot_xy(initval,fieldval,sig+" = "+line2+" pc","lc -1 pointtype "+line3);
        file = "";
        xaxis.clear();
        fieldval.clear();
        initval.clear();
        ptype = 17;
    }

/* Densidade pela massa da fonte para sig 100 e sig 200 */
    tokenstr = token.select_token(2);
    auxfid = read_header(auxfile,tokenstr,auxnf);
    cout << auxfid << "    " << auxnf << "     " << auxnh << '\n';
    var = axis(auxfile,auxfid,auxnh);

    cout << "the var values" << '\n';
    for (int i = 0; i < var.size(); i++) {cout << var[i] << '\n';}
    cout << '\n';

    multifig = "dens";
    multifig = multifig + "_" + figchar[2];

    ptype = 7;
    line3 = "";
    for (int l = 100; l <= 100; l = l + 100)
    {
        for (int k = ( (int) var[0]); k <= ( (int) var[1]); k = k + ( (int) var[2]))
        {
            conv_int << k << '\n';
            getline(conv_int,line1);
            conv_int << l << '\n';
            getline(conv_int,line2);
            file = dirstr+token.select_token(0)+"15/"+token.select_token(1)+"10/"+token.select_token(2)+line1+"/sig"+line2;
            file  = file + "/" + fname + exten;
            cout << file << '\n';
            copy_file(file,ofile);
            nh = get_header_size(ofile);
            nf = get_field_number(ofile,nh);
            fid = read_header(ofile,wfield[1],nf);
            xaxis = axis(ofile,fid-1,nh);
            fieldval.push_back(xaxis[xaxis.size()-1]);
            initval.push_back(k);
            cout << "=====================" << '\n';
            for (int ii = 0; ii < fieldval.size(); ii++) {cout << initval[ii] << "    " << fieldval[ii] << '\n';}
            cout << "=====================" << '\n';
        }
        for (int i = 0; i < fieldval.size(); i++) 
        {
            initval[i] = initval[i]*umass;
            fieldval[i] = fieldval[i]*udens/hmass;
        }
        g4.savetops(multifig);
        g4.set_xlabel("M_ (g)","");
        g4.set_ylabel("n (g cm^{-3})","");
        g4.set_x_format();
        g4.set_y_format();
        g4.set_legend_pos(initval[4] - 1.7E+42,fieldval[4]);
        conv_int << ptype << '\n';
        getline(conv_int,line3);
        g4.plot_xy(initval,fieldval,sig+" = "+line2+" pc","lc -1 pointtype "+line3);
        file = "";
        xaxis.clear();
        fieldval.clear();
        initval.clear();
        ptype = 17;
    }

/* Curva de rotacao gas e braco 
    xaxis = generate_vector<long int,double>(vecsize);
    yaxis.clear();
    for (int i = 0; i < xaxis.size(); i++) {xaxis[i] = xaxis[i]*16.0;}
    for (int i = 0; i < xaxis.size(); i++)
    {
        rotcurve = alpha*exp(-xaxis[i]/beta - (gamma/xaxis[i])*(gamma/xaxis[i])) + delta*exp(-xaxis[i]/epsilon - ni/xaxis[i]);
        yaxis.push_back(rotcurve);
    }
    g5.savetops("rot_curve");
    g5.set_xlabel("R_ (kpc)","");
    g5.set_ylabel("V(R) (km s^{-1})","");
    g5.plot_xy(xaxis,yaxis,"Barros et al. (2013)","");

    yaxis.clear();
    for (int i = 0; i < xaxis.size(); i++)
    {
        rotcurve = omegap*xaxis[i];
        yaxis.push_back(rotcurve);
    }
    g5.savetops("rot_curve");
    g5.set_xlabel("R_ (kpc)","");
    g5.set_ylabel("V(R) (km s^{-1})","");
    g5.plot_xy(xaxis,yaxis,"sistema espiral","");

 potencial 200 
    yaxis.clear();
    yaxis = generate_vector<int,double>(jsize);
    for (int k = 0; k < jsize; k++)
    {
        yaxis[k] = yaxis[k]-0.5;
    }

    for (int i  = 0; i < isize; i++) 
    {
        for (int j = 0; j < jsize; j++)
        {
            potval = (gconst*smass/(sqrt(2.0*pi)*sigma))*exp(-1.0*(yaxis[i]*yaxis[i]/(2.0*sigma*sigma)));
            pot.push_back(potval);
        }
    }

    g6.savetops("pot_simulation_200");
    g6.set_palette_model("RGB").set_palette("( 0 \"white\", 2 \"white\", 4 \"cyan\", 8 \"blue\")");
    g6.set_xrange(0,isize).set_yrange(0,isize).set_cbrange((gconst*smass/(sqrt(2.0*pi)*sigma))/1.0E+08,gconst*smass/(sqrt(2.0*pi)*sigma));
    g6.plot_image<double>(pot,isize,jsize); 

 potencial 100
    yaxis.clear();
    yaxis = generate_vector<int,double>(jsize);
    for (int k = 0; k < jsize; k++)
    {
        yaxis[k] = yaxis[k]-0.5;
    }

    sigma = 0.1;
    for (int i  = 0; i < isize; i++) 
    {
        for (int j = 0; j < jsize; j++)
        {
            potval = (gconst*smass/(sqrt(2.0*pi)*sigma))*exp(-1.0*(yaxis[i]*yaxis[i]/(2.0*sigma*sigma)));
            pot.push_back(potval);
        }
    }

    g7.savetops("pot_simulation_100");
    g7.set_palette_model("RGB").set_palette("( 0 \"white\", 2 \"white\", 4 \"cyan\", 8 \"blue\")");
    g7.set_xrange(0,isize).set_yrange(0,isize).set_cbrange((gconst*smass/(sqrt(2.0*pi)*sigma))/1.0E+08,gconst*smass/(sqrt(2.0*pi)*sigma));
    g7.plot_image<double>(pot,isize,jsize); */

/* Evolucao densidade 
    line3 = "";
    line3 = "i";
    varchar2.push_back(line3);
    line3 = "V_{i}";
    varchar2.push_back(line3);
    line3 = "M";
    varchar2.push_back(line3);

    line3 = "i";
    varchar4.push_back(line3);
    line3 = "V_{max}";
    varchar4.push_back(line3);
    line3 = "M";
    varchar4.push_back(line3);

    line3 = " ^o";
    varchar3.push_back(line3);
    line3 = " km s^{-1}";
    varchar3.push_back(line3);
    line3 = " g";
    varchar3.push_back(line3);

    dirstr = home + "/data_files2/";

    valor = 0.0;
    line1 = "";
    line2 = "";
    pr = 1;
    multifig = "dens_evol_new";
    for (int k = 10; k <= 50; k = k + 10)
    {
        conv_int << k << '\n';
        getline(conv_int,line1);
        file = dirstr+token.select_token(0)+"15/"+token.select_token(1)+"10/"+token.select_token(2)+line1+"/sig100";
        file  = file + "/" + fname + exten;
        cout << file << '\n';
        copy_file(file,ofile);
        nh = get_header_size(ofile);
        nf = get_field_number(ofile,nh);
        fid = read_header(ofile,wfield[0],nf);
        xaxis = axis(ofile,fid-1,nh);
        fid = read_header(ofile,wfield[1],nf);
        yaxis = axis(ofile,fid-1,nh);
        for (int i = 0; i < xaxis.size(); i++) 
        {
            xaxis[i] = xaxis[i]*utime/1.0E+9;
            yaxis[i] = yaxis[i]*udens/hmass;
        }            
        g7.savetops(multifig);
        g7.set_xlabel("t (Ganos)","");
        g7.set_ylabel("n (cm^{-3})","");
        g7.set_x_format();
        g7.set_y_format();
        g7.set_xrange(0.0001,3.0);
        g7.set_yrange(10.0,5000.0);
        g7.set_xlogscale(10.0E0);
        g7.set_ylogscale(10.0E0);
        g7.set_legend_pos("top","right");
        valor = ( (double) k)*umass;
        g7.plot_xy(xaxis,yaxis,"M = "+num2str<double>(valor,pr,sci)+" g");
        xaxis.clear();
        yaxis.clear();
        file = "";
    }
    g8.reset_plot();        
    var.clear();*/

/* Modelos multiplos */
    line3 = "";
    line3 = "i";
    varchar2.push_back(line3);
    line3 = "V_{i}";
    varchar2.push_back(line3);
    line3 = "M";
    varchar2.push_back(line3);

    line3 = "i";
    varchar4.push_back(line3);
    line3 = "V_{max}";
    varchar4.push_back(line3);
    line3 = "M";
    varchar4.push_back(line3);

    line3 = " ^o";
    varchar3.push_back(line3);
    line3 = " km s^{-1}";
    varchar3.push_back(line3);
    line3 = " g";
    varchar3.push_back(line3);

    dirstr = home + "/data_files2/";

    valor = 0.0;
    line1 = "";
    line2 = "";
    pr = 1;
    mfile.close();
    for (int j = 0; j < auxnf - 1; j++)
    {
        tokenstr = token.select_token(j);
        auxfid = read_header(auxfile,tokenstr,auxnf);
        var.clear();
        var = axis(auxfile,auxfid,auxnh);
        for (int l = 10; l <= 50; l = l + 10)
        {
            multifig = "dens_corr";
            conv_int << l << '\n';
            getline(conv_int,smassstr);
            mfile.open("parlist_velho.txt");
            for (int k = ( (int) var[0]); k <= ( (int) var[1]); k = k + ( (int) var[2]))
            {
                cout << k << "    " << var[1] << "    " << j << '\n';
                conv_int << k << '\n';
                getline(conv_int,fstr);
                line1 = "";
                line2 = "";
                valor = 0.0;
                if (tokenstr == "ang")
                {
                    file = dirstr+token.select_token(0)+fstr+"/"+token.select_token(1)+"10/"+token.select_token(2)+smassstr+"/sig100";
                } else 
                {
                    file = dirstr+token.select_token(0)+"15/"+token.select_token(1)+fstr+"/"+token.select_token(2)+smassstr+"/sig100";
                }
                file  = file + "/" + fname + exten;
                cout << file << '\n';
                copy_file(file,ofile);
                nh = get_header_size(ofile);
                nf = get_field_number(ofile,nh);
                dats = get_data_size(ofile,nh);
                fid = read_header(ofile,wfield[1],nf);
                valor = get_field_val<double>(ofile,nh,fid-1,dats);
                fieldval.push_back(valor);
                valor = 0.0;
                if (tokenstr == "ang")
                {
                    initval.push_back(k);
                } else
                { 
                    getline(mfile,line1);
                    token3.re_init(line1);
                    line2 = token3.select_token(0);
                    valor = str2num<double>(line2);
                    initval.push_back(valor);
                }
                cout << "=====================" << '\n';
                for (int ii = 0; ii < fieldval.size(); ii++) {cout << initval[ii] << "    " << fieldval[ii] << '\n';}
                cout << "=====================" << '\n';
            }
            for (int i = 0; i < fieldval.size(); i++){fieldval[i] = fieldval[i]*udens/hmass;}
            multifig = multifig + tokenstr;
            g8.savetops(multifig);
            if (tokenstr == "ang")
            {
                g8.set_xlabel("i (^o)","");
            } else {g8.set_xlabel("R (kpc)","");}
            g8.set_ylabel("n (cm^{-3})","");
            g8.set_x_format();
            g8.set_y_format();
            g8.set_yrange(10.0,6000.0);
            g8.set_legend_pos("top","right");
            valor = ( (double) l)*umass;
            g8.plot_xy(initval,fieldval,"M = "+num2str<double>(valor,pr,sci)+" g");
            initval.clear();
            fieldval.clear();
            file = "";
            mfile.close();
        }
        g8.reset_plot();
        var.clear();
    }

/* Decaimento exponencial 
   xaxis.clear();
   yaxis.clear();
   xaxis = generate_vector<long int,double>(vecsize);
   for (int i = 0; i < xaxis.size(); i++) {xaxis[i] = xaxis[i]*16.0;}
   valor = 0.0;
   pr = 1;
   sci = "fixed";
   epsis.push_back(1.0);
   epsis.push_back(2.5);
   epsis.push_back(5.0);

   for (int j = 0; j < epsis.size(); j++)
   {
      valor = epsis[j];
      epsilons = valor;      
      for (int i = 0; i < xaxis.size(); i++)
      {
          massR = mass0*xaxis[i]*exp(-xaxis[i]/epsilons);
          yaxis.push_back(massR);
      }
      g9.savetops("mass_decay");
      g9.set_xlabel("R_ (kpc)","");
      g9.set_ylabel("M (g)","");
      g9.plot_xy(xaxis,yaxis,ep + "_{s} = " + num2str<double>(valor,pr,sci),"");
      yaxis.clear();
   }
   g9.reset_plot(); */

/* Evolucao densidade multiplos*/
    line3 = "";
    line3 = "i";
    varchar2.push_back(line3);
    line3 = "V_{i}";
    varchar2.push_back(line3);
    line3 = "M";
    varchar2.push_back(line3);

    line3 = "i";
    varchar4.push_back(line3);
    line3 = "V_{max}";
    varchar4.push_back(line3);
    line3 = "M";
    varchar4.push_back(line3);

    line3 = " ^o";
    varchar3.push_back(line3);
    line3 = " km s^{-1}";
    varchar3.push_back(line3);
    line3 = " g";
    varchar3.push_back(line3);

    line3 = "max velo.";
    for (int j = 0; j < auxnf - 1; j++)
    {
        tokenstr = token.select_token(j);
        auxfid = read_header(auxfile,tokenstr,auxnf);
        var = axis(auxfile,auxfid,auxnh);
        for (int l = 100; l <= 100; l = l + 100)
        {
            multifig = "vel_evol";
            conv_int << l << '\n';
            getline(conv_int,line2);
            multifig = multifig + "_sig" + line2 + "_" + figchar[j];        
            for (int k = ( (int) var[0]); k <= ( (int) var[1]); k = k + ( (int) var[2]))
            {
                conv_int << k << '\n';
                getline(conv_int,line1);
                if (tokenstr == "ang")
                {
                    file = dirstr+token.select_token(0)+line1+"/"+token.select_token(1)+"10/"+token.select_token(2)+"50/sig"+line2;
                } else 
                {
                    file = dirstr+token.select_token(0)+"15/"+token.select_token(1)+line1+"/"+token.select_token(2)+"50/sig"+line2;
                }
                file  = file + "/" + fname + exten;
                cout << file << '\n';
                copy_file(file,ofile);
                nh = get_header_size(ofile);
                nf = get_field_number(ofile,nh);
                fid = read_header(ofile,wfield[0],nf);
                xaxis = axis(ofile,fid-1,nh);
                fid = read_header(ofile,line3,nf);
                yaxis = axis(ofile,fid-1,nh);
                for (int i = 0; i < xaxis.size(); i++) 
                {
                    xaxis[i] = xaxis[i]*utime/1.0E+9;
                    yaxis[i] = yaxis[i]*10.0;
                }            
                g10.savetops(multifig);
                g10.set_xlabel("t (Ganos)","");
                g10.set_ylabel("n (cm^{-3})","");
                g10.set_x_format();
                g10.set_y_format();
                g10.set_legend_pos("top ","right");
                g10.set_xrange(0.0,3.0);
                g10.set_yrange(0.0,30.0);
//                g10.set_xlogscale(10.0E0);
//                g10.set_ylogscale(10.0E0);
                if (tokenstr == "ang")
                {
                    g10.plot_xy(xaxis,yaxis,varchar2[j]+" = "+line1+varchar3[j]);
                } else
                {
                    switch (k)
                    {
                        case 5:
                            g10.plot_xy(xaxis,yaxis,"V_{i} =  "+num2str<int>(k,pr,sci)+" km s^{-1}");
                            break;
                        default:
                            g10.plot_xy(xaxis,yaxis,"V_{i} = "+num2str<int>(k,pr,sci)+" km s^{-1}");
                            break;
                    }
                }
                xaxis.clear();
                yaxis.clear();
                file = "";
            }
            g10.reset_plot();
        }
        var.clear();
    } 

/* deleta o arquivo temporario*/
    if (remove(ofile.c_str()) != 0)
    {
        perror("Error deleting file!");
    } else {cout << "File successfully deleted!" << '\n';}

/* espera input do usuario para sair*/
    cout << endl << "Press ENTER to continue..." << endl;

    cin.clear();
    cin.ignore(std::cin.rdbuf()->in_avail());
    cin.get();

    return 0;
}
