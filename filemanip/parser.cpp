#include <iostream>
#include "parser.h"

using namespace std;

parser::parser()
{
    delim = ' ';
    str   = "";
}

parser::parser(const string &s)
{
    delim = ' ';
    str   = s;
}

parser::parser(const string &s, char d)
{
    delim = d;
    str   = s;
}

parser::parser(const char d)
{
    delim = d;
    str   = "";
}

void parser::re_init(const string &s)
{
    str   = "";
    str   = s;
}

void parser::re_init(const char d)
{
    delim = d;
}

void parser::re_init(const string &s, const char d)
{
    delim = d;
    str   = "";
    str   = s;
}

int parser::ntoken()
{
    int number = 0;
    elems = tokenize();
    number  = elems.size();
    elems.clear();
    return number;
}

string parser::select_token(int l)
{
    string token = "";
    elems = tokenize();
    if (l < elems.size())
    {
        token = elems[l];
    }
    elems.clear();
    return token;
}

vector<string> parser::tokenize()
{
    stringstream ss(str);
    string item;
    while (getline(ss,item,delim))
    {
        if (!item.empty())
        {
            elems.push_back(item);
        }
    }
    return elems;
}
