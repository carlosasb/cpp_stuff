#include "filemanip.h"

using namespace std;

/*---------------------------------------------------------//
// Function AXIS: reads data from a named field in a data  //
//                file.                                    //
//                                                         //
// Arguments    : s1 => pointer to the string containing   //
//                      the data filename                  //
//                s2 => string containing the field name   //
//                      to be used                         //
//---------------------------------------------------------*/

vector<double> get_field( string s2)
{
    char dlm = ' ';
    int n = 0, i = 0, k = 0;
    double val = 0.0;
    string line = "", field = "";
    parser token(dlm);
    ifstream file(filename.c_str());
    vector<double> vec;

    /* read the first line*/
    getline(file,line);
    token.re_init(line);
    if (!line.empty())
    {
        /* the while loop goes through every token
           it finds s2*/
        n = token.ntoken();
        while (i < n)
        {
            field = token.select_token(i);
            if (field == s2)
            {
                /* reads the file until finding EOF
                   putting the values from the field
                   to a vector*/
                while (getline(file,line))
                {
                    token.re_init(line);
                    field = token.select_token(0);
                    if (isnum<float>(field))
                    {
                        field = token.select_token(i-1);
                        stringstream(field) >> val;
                        vec.push_back(val);
                    }
                }
                i = n;
            } else {i++;}
        }
    }
    file.close();

    return vec;
} //functin axis
/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function FILETEST: function tests the contents of a     //
//                    file.                                //
//                                                         //
// Arguments         : s1 => pointer to the string         //
//                           containing the data filename  //
//---------------------------------------------------------*/

bool filetest(string &s1)
{
    char dlm = ' ';
    bool no_err = false;
    ifstream file(s1.c_str());
    string line, stest;
    parser token(dlm);
    vector <string> vec;
    
    if (file.is_open())
    {
        no_err = true;
        getline(file,line);
        getline(file,line);
        while (getline(file,line) && no_err)
        {
            token.re_init(line);
            stest = token.select_token(0);
            if (isnum<float>(stest))
            {
                vec = token.tokenize();
                for (int i = 0; i < vec.size(); i++)
                {
                    if (isnum<float>(vec[i]))
                    {
                        no_err = true;
                    } else 
                    {
                        no_err = false;
                        i = vec.size();
                    }
                }
            }
        }
        file.close();
    }

    return no_err;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function COPY_FILE: function copies data files from a   //
//                     location.                           //
//                                                         //
// Arguments         : s1 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//                     s2 => pointer to the string         //
//                           containing the auxiliary      //
//                           filename                      //
//                     s3 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//---------------------------------------------------------*/

void copy_file(string &s1, string &s2, string &s3)
{
    int i = 0;
    ifstream infile(s1.c_str()), auxfile(s2.c_str());
    ofstream outfile(s3.c_str());
    string line, field;
    vector<string> header;
    parser token1;

    if (auxfile.is_open())
    {
        while (getline(auxfile,line)) {header.push_back(line);}
        auxfile.close();
    }

    while (getline(infile,line))
    {
        token1.re_init(line);
        field = token1.select_token(0);
        if (!isnum<float>(field))
        {
            outfile << header[i] << '\n';
            i++;
        } else {outfile << line << '\n';}
    }

    outfile.close();
}

/*---------------------------------------------------------*/
