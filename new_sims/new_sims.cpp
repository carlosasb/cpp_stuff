#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

#include "parser.h"
#include "gnuplot_i.hpp"

/*---------------------------------------------------------//
// Function STR2NUM: function (templated) converts a       // 
//                   string to a numerical value           //
//                                                         //
// Arguments       :  s=> pointer to the string to be      //
//                        converted                        //
//---------------------------------------------------------*/

template <class numtype>
numtype str2num(string &s)
{
    numtype num;

    stringstream(s) >> num;
    
    return num;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function NUM2STR: function (templated) converts a       //
//                   numerical value to a string           //
//                                                         //
// Arguments       :num  => pointer to the numerical value //
//                          to be converted                //
//                 :prstn=> ponter to the precision of the //
//                          floating point                 //
//---------------------------------------------------------*/

template <class numtype>
string num2str(numtype &num, int &prstn, string &sci)
{
    stringstream conv;
    string str;
    
    if (sci == "fixed")
    {
        conv << fixed << setprecision(prstn) << num << '\n';
    } else
    {
        conv << scientific << setprecision(prstn) << num << '\n';
    }
    getline(conv,str);

    return str;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function ISNUM: function tests a string for a numerical //
//                  value (Templated for the type for      //
//                  testting).                             //
//                                                         //
// Arguments     : s => pointer to the string to be        //
//                      tested                             //
//---------------------------------------------------------*/

template <class numtype>
bool isnum(string &s1)
{
    istringstream iss(s1);
    /* numtype can be int, float and double;
       non-numeric types always will be false*/
    numtype val;
    /* reads from the stream; noskipws considers 
       invalid any leading whitespaces*/
    iss >> noskipws >> val;
    /* tests the stream for being finished
       and failure to pass the value*/
    return iss.eof() && !iss.fail();
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GENERATE_VECTOR: generates a float vector      //
//                           (templated for the integer    //
//                           type).                        //
//                                                         //
// Arguments               : n => pointer to the inttype   //
//                                integer                  //
//---------------------------------------------------------*/

template <class inttype, class T>
vector<T> generate_vector(inttype &n)
{
    int val = 0, k = 0, vecsize = 0;
    T valf = 0.0;
    vector<T> valvec, fvec;

    for (int i = 1; i <= n; i++) 
    {
        val = rand() % n;
        valf = val*1.0/n;
        for (int j = 0; j < valvec.size(); j++)
        {
            if (valf == valvec[j]) 
            {
                val  = rand() % n;
                valf = val*1.0/n;
                j = -1;
            }
        }
        valvec.push_back(valf);
    }
    valvec.push_back(1);

    vecsize = valvec.size();
    for (int i = 0; i < vecsize; i++)
    {
        valf = valvec[k];
        for (int j = 0; j < valvec.size(); j++) 
        {
            if (valf > valvec[j]) 
            {
                valf = valvec[j];
                k = j;
            }
        }
        fvec.push_back(valf);
        valvec.erase(valvec.begin()+k);
        k = 0;
    }

    return fvec;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function READ_HEADER: function reads the header of a    //
//                       data file.                        //
//                                                         //
// Arguments           : s1  => pointer to the string      //
//                              containing the data        //
//                              filename                   //
//                       s2  => pointer to the string      //
//                              containing the field name  //
//                       num => number of fields           //       
//---------------------------------------------------------*/

int read_header(string &s1, string &s2, int num)
{
    char charstrm;
    int tindex = 0, j = 0, n = 0;
    ifstream file(s1.c_str());
    stringstream iss;
    string line = "", field = "", strstrm = "", auxstr = "";
    parser token;
    vector<string> header;

    getline(file,line);
    token.re_init(line);
    /* tests to see if number of tokens in the 
       first line of s1 file is the same as 
       the number of fields; if it is selects
       the s2 field; else finds the right position
       for the s2 field*/
    if (token.ntoken() == num)
    {
        field = token.select_token(0);
        while (field != s2)
        {
            j++;
            field = token.select_token(j);
        }
    } else
    {
        iss << line;
        for (int i = 0; i < token.ntoken(); i++)
        {
            field = token.select_token(i);
            if (i == 0)
            {
                n = field.size();
            } else {n = field.size() - 1;}
            for (int k = 1; k <= n; k++) {iss.get(charstrm);}
            iss.get(charstrm);
            strstrm = charstrm;
            while (strstrm == " ")
            {
                iss.get(charstrm);
                strstrm = charstrm;
                j++;
            }
            if (j == 1)
            {
                do
                {
                    field = field+" "+token.select_token(i+1);
                    j = 0;
                    i++;
                    auxstr = token.select_token(i);
                    if (i == 0)
                    {
                        n = auxstr.size();
                    } else {n = auxstr.size() - 1;}
                    for (int k = 1; k <= n; k++) {iss.get(charstrm);}
                    iss.get(charstrm);
                    strstrm = charstrm;
                    while (strstrm ==" ")
                    {
                        iss.get(charstrm);
                        strstrm = charstrm;
                        j++;
                    }
                } while (j == 1);
                header.push_back(field);
                j = 0;
            } else
            {
                header.push_back(field);
                j = 0;
            }
        }
        j = 0;
        for (int l = 0; l < header.size()-1; l++)
        {
            if (header[l] == s2) {j = l;}
        }
    }

    file.close();

    tindex = j;

    return tindex;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_HEADER_SIZE: function gets the number of   //
//                           file.                         //
//                                                         //
// Arguments               : s => pointer to the string    //
//                                 containing the data     //
//                                 filename                //
//---------------------------------------------------------*/

int get_header_size(string &s)
{
    int hsize = 0;
    /* isheader is true if reading header from s file
       or false if reading from data in s file*/
    bool isheader = true;
    ifstream file(s.c_str());
    string line = "", field;
    parser token;

    /* reads a line from s file until file is over
       or if isheader is false*/
    while (getline(file,line) || isheader)
    {
        token.re_init(line);
        field = token.select_token(0);
        /* tests to see if the new line is of data or
           header; if data isheader is false*/
        if (isnum<float>(field)) 
        {
            isheader = false;
        } else
        {
            hsize++;
        }
        
    }

    file.close();

    return hsize;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_DATA_SIZE: function gets the number of     //
//                         data points                     //
//                                                         //
// Arguments             : s   => pointer to the string    //
//                                containing the data      //
//                                filename                 //
//                         num => header size              //
//---------------------------------------------------------*/

int get_data_size(string &s, int &num)
{
    int dat_size = 0, size = 0;
    string garbage = "";
    ifstream file(s.c_str());
    /* reads the entire file and defines the number of lines */
    while (getline(file,garbage)) {size++;}
    
    dat_size = size - num;
    file.close();
    
    return dat_size;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_FIELD_NUMBER: function gets the number of  //
//                            fields.                      //
//                                                         //
// Arguments                : s   => pointer to the string //
//                                   containing the data   //
//                                   filename              //
//                            num => header size           //
//---------------------------------------------------------*/

int get_field_number(string &s, int num)
{
    ifstream file(s.c_str());
    string line = "";
    parser token;

    /* reads header and discards it*/
    for (int i = 0; i <= num-1; i++) {getline(file,line);}

    /* reads the first data line in s file;
       counts the number of tokens in line*/
    getline(file,line);
    token.re_init(line);

    file.close();

    return token.ntoken();
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function GET_FIELD_VAL: reads data from a specific      //
//                         position on a named field       //
//                         in a data file (templated)      //
// Arguments             : s1  => pointer to the string    //
//                                containing the data      //
//                                filename                 //
//                         num1=> header size              //
//                         num2=> field identifier         //
//                         num3=> position on field        //
//---------------------------------------------------------*/

template <class numtype>
numtype get_field_val(string &s, int &num1, int num2, int &num3)
{
    ifstream file(s.c_str());
    numtype val;
    string line = "", val_str = "", header = "";
    parser token;
    /* reads and discards the header */
    for (int i = 1; i <= num1; i++){getline(file,header);}
    /* reads and discards the data points prior to num3 */
    for (int i = 1; i < num3; i++){getline(file,line);}
    line = "";
    
    getline(file,line);
    token.re_init(line);
    val_str = token.select_token(num2);
    stringstream(val_str) >> val;
    file.close();    

    return val;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function AXIS: reads data from a named field in a data  //
//                file.                                    //
//                                                         //
// Arguments    : s1  => pointer to the string containing  //
//                       the data filename                 //
//                num1=> field identifier                  //
//                num2=> header size                       //
//---------------------------------------------------------*/

vector<double> axis(string &s1, int num1, int num2)
{
    double val = 0.0;
    string line = "", field = "";
    parser token;
    ifstream file(s1.c_str());
    vector<double> vec;

    /* read the the header*/
    for (int i = 1; i <= num2; i++) {getline(file,line);}

    /* get the values from the specific field*/
    while (getline(file,line))
    {
        token.re_init(line);
        field = token.select_token(num1);
        stringstream(field) >> val;
        vec.push_back(val);
    }
    file.close();

    return vec;
} 
/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function FILETEST: function tests the contents of a     //
//                    file.                                //
//                                                         //
// Arguments         : s1  => pointer to the string        //
//                            containing the data filename //
//                     num => header size                  //
//---------------------------------------------------------*/

bool filetest(string &s1, int num)
{
    bool no_err = false;
    ifstream file(s1.c_str());
    string line = "";
    parser token;
    vector <string> vec;
    
    if (file.is_open())
    {
        no_err = true;
        /* reads and discards the header*/
        for (int i = 1; i <= num; i++) {getline(file,line);}
        while (getline(file,line) && no_err)
        {
            token.re_init(line);
            vec = token.tokenize();
            cout << token.ntoken() << '\n';
            for (int j = 0; j < vec.size(); j++)
            {
                if (isnum<float>(vec[j]))
                {
                    no_err = true;
                } else 
                {
                    no_err = false;
                    j = vec.size();
                }
            }
        }
        file.close();
    }

    return no_err;
}

/*---------------------------------------------------------*/

/*---------------------------------------------------------//
// Function COPY_FILE: function copies data files from a   //
//                     location.                           //
//                                                         //
// Arguments         : s1 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//                     s2 => pointer to the string         //
//                           containing the new data       //
//                           filename                      //
//---------------------------------------------------------*/

void copy_file(string &s1, string &s2)
{
    int i = 0;
    ifstream infile(s1.c_str());
    ofstream outfile(s2.c_str());
    string line, field;
    parser token1;

    while (getline(infile,line))
    {
        token1.re_init(line);
        field = token1.select_token(0);
        outfile << line << '\n';
        i++;
    }

    infile.close();
    outfile.close();
}

/*---------------------------------------------------------*/

int main(int argc, char* argv[])
{
    /* Gnuplot class objects */
    Gnuplot g1("linespoints"), g2("lines"), g3("points"), g4;

    /* integer variables */
    int auxnf = 0, auxnh = 0, auxfid = 0, m = 0, nf = 0, nh = 0, fid = 0, val = 0, ptype = 7;
    int isize = 1024, jsize = 1024, ii = 0, pr = 1, dats = 0, tindex = 0, arrownum = 16;
    int ijumpv = isize/arrownum, jjumpv = jsize/arrownum;
    long int vecsize = 100000;

    /* float or double variables */
    double udens = 1.92E-23, hmass = 1.6733E-24, utime = 9.79E+7, uvel = 10.0;
    double valor = 0.0, tc = 0.0;

    /* string class objects */
    string line = "", file = "", fname = "statistics_01", figname = "dens_corr";
    string ofile = "stat.dat", newstr = "_sig", exten = ".dat", multifig = "dens";
    string sig = "{/Symbol s}", dirstr = "", figexten = ".eps";
    string auxfile = "stat2.txt", line1 = "", line2 = "", tokenstr, line3 = "";
    string sigstr = "", smassstr = "", fstr = "", fname2 = "stat", gammastr = "";
    string gammasym = "{/Symbol g}", auxfile2 = "time.txt", tstr = "", tstr2 = "";
    string tokenstr2 = "", fval = "", sci = "fixed", palettemodel = "RGB";
    string palette = "( 0 \"light-cyan\", 2 \"cyan\", 4 \"blue\", 8 \"dark-blue\", 10 \"black\")";
    string tmpfile1 = "dens.dat", tmpfile2 = "velv.dat", home = getenv("HOME");

    /* vector class objects */
    vector<double> xaxis, yaxis, densvec, tempvec, var, fieldval, initval, velvec;
    vector<string> wfield, figchar, varchar2, varchar3, varchar4;

    /* various streams */
    stringstream filestrm, conv_int;
    ifstream auxstrm(auxfile.c_str()), mfile, test_file, densfile, velvfile;

    /* parser class objects */
    parser token, token2, token3;

    for (int j = 1; j < argc; j++)
    {
        filestrm << argv[j] << '\n';
        getline(filestrm,line);
        wfield.push_back(line);
        cout << line << '\n';
    }

    g1.set_font("{},14");
    g2.set_font("{},14");
    g3.set_font("{},14");
    g4.set_font("{},14");

    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "Densidade maxima pelo rio galactico para sig = 100 pc e 200 pc" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';

/* densidade maxima pelo raio galactico para 100 e 200 pc */
    getline(auxstrm,line);
    auxstrm.close();
    auxnh = get_header_size(auxfile);
    auxnf = get_field_number(auxfile,nh);
    token.re_init(line);
    tokenstr = token.select_token(1);
    auxfid = read_header(auxfile,tokenstr,auxnf);
    var = axis(auxfile,auxfid,auxnh);

    dirstr = home + "/data_files/";

    for (int l = 100; l <= 200; l = l + 100)
    {
        sigstr = num2str<int>(l,pr,sci);
        mfile.open("galactic_radius_list.txt");
        for (int k = -40; k <= 40; k = k + 10)
        {
            fstr = num2str<int>(k,pr,sci);
            file  = dirstr+token.select_token(0)+"11/ang15/"+token.select_token(1)+fstr+"/smass250/"+token.select_token(2)+sigstr+"/";
            file = file + fname + exten;
            cout << file << '\n';
            copy_file(file,ofile);
            nh = get_header_size(file);
            nf = get_field_number(file,nh);
            fid = read_header(file,wfield[1],nf);
            dats = get_data_size(file,nh);
            valor = get_field_val<double>(file,nh,fid-1,dats);
            getline(mfile,line1);
            token3.re_init(line1);
            if (l == 100 && k == 0)
            {
               cout << "Ignoring" << '\n';
            } else
            {
                fieldval.push_back(valor);
                valor = 0.0;
                line2 = token3.select_token(0);
                valor = str2num<double>(line2);
                initval.push_back(valor);
                velvec.push_back(k);
            }
            cout << "=====================" << '\n';
            for (int ii = 0; ii < fieldval.size(); ii++) {cout << initval[ii] << "    " << fieldval[ii] << "    " << velvec[ii] << '\n';}
            cout << "=====================" << '\n';
        }
        for (int i = 0; i < fieldval.size(); i++) {fieldval[i] = fieldval[i]*udens/hmass;}
        g1.savetops(figname);
        g1.set_xlabel("R (kpc)","");
        g1.set_ylabel("n (g cm^{-3})","");
        g1.set_x_format();
        g1.set_y_format();
        g1.set_yrange(10.0,3000.0);
        g1.set_xrange(6.0,11.0);
        g1.set_ylogscale(10.0E0);
        g1.set_legend_pos("top","right");
        g1.plot_xy(initval,fieldval,sig+" = "+sigstr+" pc");
        mfile.close();
        fieldval.clear();
        initval.clear();
    }

    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "Densidade maxima pelo tempo" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';

/* densidade maxima pelo tempo */
    file = "";
    for (int gamma = 11; gamma <= 12; gamma++)
    {
        gammastr = "gamma" + num2str<int>(gamma,pr,sci);
        multifig = multifig + "_max_" + gammastr;
        for (int l = 100; l <= 200; l += 100)
        {
            sigstr = "sig" + num2str<int>(l,pr,sci);
            for (int k = ( (int) var[0]); k <= ( (int) var[1]); k += ( (int) var[2]))
            {
                fstr = tokenstr + num2str<int>(k,pr,sci);
                file = dirstr +  gammastr + "/ang15/" + fstr + "/smass250/" + sigstr + "/";
                file = file + fname + exten;
                cout << file << '\n';
                test_file.open(file.c_str());
                cout << "Began testing " + file << '\n';
                if (test_file.is_open())
                {
                    test_file.close();
                    if (l == 100 && k == 0 && gamma == 11)
                    {
                       cout << "Ignoring " + file << '\n';
                       cout << "===========================================================================" << '\n';
                    } else
                    {
                        cout << "Began copying " + file << '\n';
                        copy_file(file,ofile);
                        nh = get_header_size(file);
                        nf = get_field_number(file,nh);
                        fid = read_header(file,wfield[0],nf);
                        cout << "Began " + wfield[0] + " extraction" << '\n';
                        xaxis = axis(ofile,fid-1,nh);
                        nh = get_header_size(ofile);
                        nf = get_field_number(ofile,nh);
                        fid = read_header(ofile,wfield[1],nf);
                        cout << "Began " + wfield[1] + "extraction" << '\n';
                        yaxis = axis(ofile,fid-1,nh);
                        cout << "===========================================================================" << '\n';
                        for (int i = 0; i < xaxis.size(); i++) 
                        {
                            xaxis[i] = xaxis[i]*utime/1.0E+9;
                            yaxis[i] = yaxis[i]*udens/hmass;
                        }
                        /* plots go here*/
                        g2.savetops(multifig);
                        g2.set_xlabel("t (Ganos)","");
                        g2.set_ylabel("n (cm^{-3})","");
                        g2.set_x_format();
                        g2.set_y_format();
			g2.set_xrange(0.001,3.0);
                        g2.set_yrange(10.0,5000.0);
                        g2.set_xlogscale(10.0E0);
                        g2.set_ylogscale(10.0E0);
                        g2.set_xtics("(\"0.001\" 0.001,\"0.01\" 0.01,\"0.1\" 0.1,\"1\" 1,\"3\" 3)");
                        g2.set_legend_pos("top","right");
                        switch (k)
                        {
                            case -10:
                                g2.plot_xy(xaxis,yaxis,"V_{i} = "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                            case 10:
                                g2.plot_xy(xaxis,yaxis,"V_{i} =  "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                            case 0:
                                g2.plot_xy(xaxis,yaxis,"V_{i} =   "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                            defaut:
                                cout << "ignoring..." << '\n';
                                break;
                        }
                        /* reseting vector here*/
                        xaxis.clear();
                        yaxis.clear();
                    }
                } else 
                {
                    cout << "Error while reading " + file + "!" << '\n';
                    cout << "File not found!" << '\n';
                }
            }
        }
        multifig = "dens";
        /*reseting plots here*/
        g2.reset_plot();
    }

    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "Perfis de densidade ou velocidade" << '\n';
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';
    cout << "" << '\n';

/* Profiles */
    pr = 2;
    valor = 0.0;
    file = "";
    line = "";
    multifig = "velc";
    fname = multifig + "_slices_xy";
    dirstr = home + "/hdf5_data_extractor/resultados_temp/testes_data_extraction/datfiles/";

    auxstrm.open(auxfile2.c_str());
    getline(auxstrm,line);
    auxstrm.close();

    token.re_init(line);

    /* reseting vector here*/
    xaxis.clear();
    yaxis.clear();
  
    for (int i = 0; i < token.ntoken(); i++)
    {
        tokenstr2 = token.select_token(i);
        tindex = str2num<int>(tokenstr2);
        tindex = ( (int) tindex/20);
        tstr = num2str<int>(tindex,pr,sci);
        tstr2 = "time" + tstr;
        multifig = multifig + tstr;
        for (int l = 100; l <= 200; l += 100)
        {
            sigstr = "sig" + num2str<int>(l,pr,sci);
            for (int k = ( (int) var[0]); k <= ( (int) var[1]); k += ( (int) var[2]))
            {
                fstr = tokenstr + num2str<int>(k,pr,sci);
                file = dirstr + fname + "_gamma11_" + tstr2 + "_" + fstr + "_" + sigstr;
                file = file + exten;
                cout << file << '\n';
                
                cout << "Began copying " + file << '\n';
                copy_file(file,ofile);
                mfile.open(file.c_str());
                cout << "Began testing " + file << '\n';
                if (mfile.is_open())
                {
                    mfile.close();
                    mfile.open(ofile.c_str());
                    if (l == 100 && k == 0)
                    {
                       cout << "Ignoring " + file << '\n';
                       cout << "===========================================================================" << '\n';
                    } else
                    {
                        cout << "Began profile extraction" << '\n';
                        line = "";
                        while (getline(mfile,line))
                        {
                            token2.re_init(line);
                            fval = token2.select_token(0);
                            valor = str2num<double>(fval);
                            xaxis.push_back(valor);
                            fval = token2.select_token(1);
                            valor = str2num<double>(fval);
                            yaxis.push_back(valor);
                        }
                        cout << "===========================================================================" << '\n';
                        for (int i = 0; i < xaxis.size(); i++) 
                        {
                            xaxis[i] = (xaxis[i] - 512.0)/1024.0;
                            yaxis[i] = yaxis[i]*10.0;
                        }
                        /* plots go here */
                        g3.savetops(multifig);
                        g3.set_xlabel("Y (kpc)","");
                        g3.set_ylabel("V_{y} (km s^{-1})","");
                        tc = ( (double) tindex)*0.2*utime/1.0E+9;
                        if (tc < 0.1)
                        {
                            g3.set_title("Integration time = "+num2str<double>(tc,pr,sci)+" Gyrs");
                        } else
                        {
                            pr = 1;
                            g3.set_title("Integration time = "+num2str<double>(tc,pr,sci)+" Gyrs");
                        }
                        pr = 2;
                        g3.set_x_format();
                        g3.set_y_format();
                        g3.set_yrange(-10.0,20.0);
                        g3.set_xrange(-0.5,0.5);
                        g3.set_legend_pos("top","right");
                        switch (k)
                        {
                             case -10:
                                g3.plot_xy(xaxis,yaxis,"V_{i} = "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                            case 10:
                                g3.plot_xy(xaxis,yaxis,"V_{i} =  "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                            case 0:
                                g3.plot_xy(xaxis,yaxis,"V_{i} =   "+num2str<int>(k,pr,sci)+" km s^{-1} and "+sig+" = "+num2str<int>(l,pr,sci)+" pc");
                                break;
                        }
                        /* reseting vector here */
                        xaxis.clear();
                        yaxis.clear(); 
                    }
                } else 
                {
                    cout << "Error while reading " + file + "!" << '\n';
                    cout << "File not found!" << '\n';
                } 
            }
        }
        multifig = "velc";
        /* reseting plots here*/
        g3.reset_plot();
    }

/* deleta o arquivo temporario*/
    if (remove(ofile.c_str()) != 0)
    {
        perror("Error deleting file!");
    } else {cout << "File successfully deleted!" << '\n';}

/* espera input do usuario para sair*/
    cout << endl << "Press ENTER to continue..." << endl;

    cin.clear();
    cin.ignore(std::cin.rdbuf()->in_avail());
    cin.get();

    return 0;
}
